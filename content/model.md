# Datamodell

Datamodellen är tabulär, det innebär att varje rad motsvarar exakt en rapportering av matsvinn och varje kolumn motsvarar en egenskap för det matsvinnet. 18 attribut är definierade, där de första 6 är obligatoriska. 

<div class="note" title="1">
Vi väljer att använda beskrivande men korta kolumnnamn som uttrycks med gemener, utan mellanslag (understreck för att separera ord) och på engelska.

Genom engelska attributnamn blir modellen enklare att hantera i programvaror och tjänster utvecklade utanför Sverige. 

Notera att beroende på typ av kök så kan vissa typer av svinn utebli. T.ex. ett serveringskök tar emot tillagad mat och serverar det enbart, det sker ingen matlagning i köket och kan således inte ha kökssvinn. Ange nollvärde. 

Notera att det i datamodellen finns fyra olika typer av kök vilket bygger på systemleverantörernas definitioner. Ange den typ av kök, som passar bäst in på er verksamhet.


</div>

<div class="note" title="2">


<div class="ms_datatable">

| Namn  | Kardinalitet      | Datatyp                         | Beskrivning|
| -- | :---------------:| :-------------------------------: | ---------------------- |
|[**source**](#source)|1|[heltal](#heltal)|**Obligatoriskt** - Ange organisationsnummret utan mellanslag eller bindesstreck för organisationen som hanterar matsvinnet. |
|[**id**](#id)|1|heltal|**Obligatoriskt** - Anger en identifierare för matsvinnet. Lämpligen använd datum skrivet som YYYYMMDD för det rapporterade matsvinnet. |
|[**name**](#name)|1|text|**Obligatoriskt** - Anger namn på verksamheten där matsvinn skapats. |
|[**serving_waste**](#serving_waste)|1|decimal|**Obligatoriskt**- Anger serveringssvinn i kilogram med en decimal. |
|[**plate_waste**](#plate_waste)|1|decimal|**Obligatoriskt** - Anger tallrikssvinn i kilogram med en decimal.|
|[**kitchen_waste**](#kitchen_waste)|1|decimal|**Obligatoriskt**- Anger kökssvinn i kilogram med en decimal. |
|[category](#category)|0..1|text|Anger kategori på verksamhet som skapat matsvinn, t.ex. förskola. |
|[kitchen_type](#kitchen_type)|0..1|text|Anger typ av kök vid verksamheten; _Produktionskök, tillagningskök, mottagningskök & serveringskök_. Notera att typ av kök kan påverka mängden svinn.|
|[meal_type](#meal_type)|0..1|text|Anger vilken typ av måltid som serverats; _Frukost, mellanmål, lunch, middag_. |
|[total_prepared_food](#total_prepared_food)|0..1|decimal|Anger total mängd preparerad mat i kilogram med en decimal. Viktig data för att kunna räkna bort sparad mat och sedan räkna ut totala mängden konsumerad mat och totala mängden svinn. | 
|[total_waste](#total_waste)|0..1|decimal|Anger total mängd matvsinn i kilogram med en decimal. |
|[storage_waste](#storage_waste)|0..1|decimal|Anger lagringssvinn i kilogram med en decimal. |
|[preparation_waste](#preparation_waste)|0..1|decimal|Anger förberedelsesvinn i kilogram med en decimal. |
|[cooking_waste](#cooking_waste)|0..1|decimal|Anger tillagningssvinn i kilogram med en decimal. |
|[special_diet_waste](#saved_food)|0..1|decimal|Anger mängden specialkostsvinn i antal kilogram med en decimal.  |
|[description](#description)|0..1|text|Anger en kortare beskrivning av matsvinnen. |
|[amount_eating](#amount_eating)|0..1|heltal|Anger antalet som ätit datumet då matsvinn skapats. |
|[saved_food](#saved_food)|0..1|decimal|Anger sparad mat i antal kilogram med en decimal. |
|[date](#date)|0..1|[dateTime](#datetime)| Ange datumet för den dag som det registrerade svinnet avser enligt [ISO 8601](https://www.iso.org/iso-8601-date-and-time-format.html).
|[updated](#updated)|0..1|text|Ange när matsvinnen uppdaterades senast.|
|[email](#email)|0..1|text|E-postadress för vidare kontakt, anges med gemener och med @ som avdelare.|
|[URL](#url)|0..1|[URL](#url)|Ingångssida för mer information om matsvinnen.|


</div>

## Förtydligande av datatyper

En del av datatyperna nedan förtydligas med hjälp av det som kallas reguljära uttryck. Dessa är uttryckta så att de matchar exakt, d.v.s. inga inledande eller eftersläpande tecken tillåts.

### **heltal**
Reguljärt uttryck: **`/^\-?\\d+$/`**

Heltal anges alltid som en radda siffror utan mellanrum eventuellt med ett inledande minus. Se [xsd:integer](https://www.w3.org/TR/xmlschema-2/#integer) för en längre definition. Utelämnat värde tolkas aldrig som noll (0) utan tolkas som “avsaknad av värde”.
 
### **decimal**
Reguljärt uttryck: **`/^\-?\\d+\\.\\d+$/`**

Decimaltal anges i enlighet med [xsd:decimal](https://www.w3.org/TR/xmlschema-2/#decimal). Notera att i Sverige används ofta decimalkomma inte punkt. För att vara enhetlig mellan olika dataformat ska decimalpunkt användas då den tabulära modellen använder komma som separator.

Den kanoniska representationen i xsd:decimal är påbjuden, d.v.s. inga inledande nollor eller +, samt att man alltid ska ha en siffra innan och efter decimalpunkt. Noll skrivs som 0.0 och ett utelämnat värde skall aldrig tolkas som noll (0) utan “avsaknad av värde”.

### **url**

En länk till en webbsida där matsvinnen presenteras hos den lokala myndigheten eller arrangören.

Observera att man inte får utelämna schemat, d.v.s. "www.example.com" är inte en tillåten webbadress, däremot är "https://www.example.com" ok. Relativa webbadresser accepteras inte heller. (Ett fullständigt reguljärt uttryck utelämnas då den är både för omfattande och opedagogisk.)

Om du behöver ange flera URL:er måste du då sätta dubbelt citattecken och separera de olika URL:erna med kommatecken. Läs gärna med i RFC 4180 för CSV.

Kontrollera även innan du publicerar filen att det går att läsa in din fil utan problem. Det finns flera webbverktyg för att testa så kallad [parsing](https://sv.wikipedia.org/wiki/Parser) t.ex. [https://CSVLint.io](https://csvlint.io) som du kan använda för att testa din fil.

### **boolean**

Reguljärt uttryck: **`/^[true|false]?&/`**

I samtliga förekommande fall kan texten “true” eller “false” utelämnas.

Ett tomt fält skall tolkas som “okänt” eller “ej inventerat”. Ange enbart “true” eller “false” om du vet att egenskapen finns (true) eller saknas (false). Gissa aldrig.

Attributet är en så kallad Boolesk” datatyp och kan antingen ha ett av två värden: TRUE eller FALSE, men aldrig båda.

</div>

## Förtydligande av attribut

### **id**

Reguljärt uttryck: **`/^[a-zA-Z_:1-9]*$/`**

En unik identifierare för den lediga tomten. 

### source

Ange organisationsnummret utan mellanslag eller bindesstreck för organisationen. Exempel för Gullspångs kommun: **2120001637**.

### name

Anger namn på verksamheten där matsvinn skapats. Exempelvis "Algården".

### description

Beskrivning av matsvinnen.

### kitchen_waste

Kökssvinn, även kallat tillagningssvinn som tillkommit under tillagning.

### category

Anger kategori på verksamhet som skapat matsvinn, t.ex. förskola.

### email

Funktionsadress till organisationen. Ange ej det inledande URI schemat mailto: eller HTML-koder i uttrycket. Exempel: info@organisation.se

### url 

En ingångssida. Anges med inledande schemat **https://** eller **http://** 
